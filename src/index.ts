import { configureServerAndEnv } from "@joel-muehlena-website/jm-config-server-addon";
import { NextFunction, Request, Response } from "express";
import morgan from "morgan";
import path from "path";
import fs from "fs";
import dotenv from "dotenv";
import { MongoDBConnection } from "./config/mongoDBSetup";
import { ExpressServer } from "./Server";
import { userV1Router } from "./routes/v1/user";

const configServer = `http://${
  process.env.NODE_ENV === "production"
    ? "config-server.default.svc.cluster.local"
    : "172.25.27.254"
}:8081/config?filename=user:${
  process.env.NODE_ENV === "production" ? "prod" : "dev"
}`;

(async () => {
  try {
    await configureServerAndEnv(configServer, path.join(__dirname, "/.env"));
  } catch (err) {
    console.log("Error fetching config server");
  }
})();

let count = 0;
let i = setInterval(() => {
  if (count > 5000) {
    console.log("Config Timeout");
    process.exit(-1);
  }
  fs.access(path.join(__dirname, "/.env"), fs.constants.F_OK, (err) => {
    if (!err) {
      clearInterval(i);
      runApp();
    } else {
      count++;
    }
  });
}, 500);

const runApp = () => {
  dotenv.config({ path: path.join(__dirname, "/.env") });
  const PORT: number = parseInt(process.env.PORT as string) || 3001;

  let expressServer = new ExpressServer(PORT);

  //Configure middleware
  expressServer.addMiddleware(morgan, "dev");

  //Configure Routes
  expressServer.get("/healthz", (_: Request, res: Response) => {
    res.sendStatus(200);
  });

  expressServer.addMiddleware(
    (req: Request, res: Response, next: NextFunction) => {
      res.locals.uuid = req.headers["x-request-uuid"];
      next();
    }
  );

  //User v1 routes
  expressServer.addRouteGroup("/v1", userV1Router);

  expressServer.get("*", (_: Request, res: Response) => {
    if (!res.headersSent) {
      res.status(400).json({ code: 400, msg: "This operation is not allowed" });
      throw Error("Operation not allowed");
    }
  });

  expressServer.run();

  //Connect to Atlas cluster
  let dbConnection = new MongoDBConnection();
  dbConnection.connectToMongoDB();
};
