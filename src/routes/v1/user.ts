import { Router } from "express";
import { deleteUser } from "../../common/v1/deleteUser";
import { createUser } from "../../common/v1/createUser";
import { editUser } from "../../common/v1/editUser";
import {
  getAllUsers,
  getRequestedUserList,
  getUserById,
} from "../../common/v1/getUserEntries";

export const userV1Router = Router();

userV1Router.get("/user", getAllUsers);
userV1Router.post("/user/requestList", getRequestedUserList);
userV1Router.get("/user/:id", getUserById);
userV1Router.post("/user", createUser);
userV1Router.patch("/user/:id", editUser);
userV1Router.delete("/user/:id", deleteUser);
