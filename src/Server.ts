import express, { Application, Router } from "express";
import {
  json as bodyParserJson,
  urlencoded as bodyParserUrlencoded,
} from "body-parser";
import morgan from "morgan";

export class ExpressServer {
  private PORT: number;
  public app: Application;

  constructor(port: number, dev = false) {
    this.app = express();
    this.PORT = port;
    this.config(dev);
  }

  public get(path: string, func: any): void {
    this.app.get(path, func);
  }

  public post(path: string, func: any): void {
    this.app.post(path, func);
  }

  public delete(path: string, func: any): void {
    this.app.delete(path, func);
  }

  public put(path: string, func: any): void {
    this.app.put(path, func);
  }

  private config(dev: boolean): void {
    this.app.use(bodyParserJson());
    this.app.use(bodyParserUrlencoded({ extended: false }));

    if (dev) {
      this.app.use(morgan("dev"));
    }
  }

  public addMiddleware(middlewareFunc: any, ...options: any[]) {
    if (options.length > 0) {
      this.app.use(middlewareFunc(...options));
    } else {
      try {
        this.app.use(middlewareFunc());
      } catch (err) {
        this.app.use(middlewareFunc);
      }
    }
  }

  public addRouteGroup(path: string, file: Router) {
    this.app.use(path, file);
  }

  public run() {
    this.app.listen(this.PORT, () => {
      console.log(`Server läuft auf Port: ${this.PORT}`);
    });
  }
}
