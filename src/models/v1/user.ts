import { model, Schema } from "mongoose";

const UserSchema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
    required: false,
  },
  sessionRev: {
    type: Number,
    default: 0,
  },
  roles: [
    {
      type: String,
    },
  ],
  password: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    defualt: Date.now,
  },
});

export const User = model("user", UserSchema, "users");
