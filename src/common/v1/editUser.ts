import { NextFunction, Request } from "express";
import { User } from "../../models/v1/user";
import { IUser } from "../../models/v1/user.interface";
import { ApiResponse } from "./ApiResponse";
import { UserPassword } from "./createUser";

type UserReqBody = Omit<IUser, "_id" | "createdAt" | "updatedAt"> &
  UserPassword;

export const editUser = async (
  req: Request<{ id: string }, any, Partial<UserReqBody>>,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const user: IUser | undefined = User.findById(id);
  if (!user) {
    res.status(404).json({
      code: 404,
      msg: `No user entry with the id ${id} found. Could not be edited.`,
    });

    return next();
  }

  const {
    avatar,
    email,
    firstName,
    lastName,
    password,
    roles,
    sessionRev,
    username,
  } = req.body;

  const updatedUser: Partial<IUser & UserPassword> = {};

  if (avatar) updatedUser.avatar = avatar;
  if (email) updatedUser.email = email;
  if (firstName) updatedUser.firstName = firstName;
  if (lastName) updatedUser.lastName = lastName;
  if (password) updatedUser.password = password;
  if (roles) updatedUser.roles = roles;
  if (sessionRev) updatedUser.sessionRev = sessionRev;
  if (username) updatedUser.username = username;

  updatedUser.updatedAt = new Date();

  const updatedUserData = await User.findByIdAndUpdate(
    id,
    { $set: updatedUser },
    { new: true }
  );

  if (updatedUserData) {
    res.status(200).json({ code: 200, msg: "Success", data: updatedUserData });

    return next();
  } else {
    res.status(500).json({ code: 500, msg: "Failed to update the user entry" });

    return next();
  }
};
