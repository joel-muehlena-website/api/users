import { NextFunction, Request } from "express";
import { IUser } from "src/models/v1/user.interface";
import { ApiResponse } from "./ApiResponse";
import { User } from "../../models/v1/user";

export interface UserPassword {
  password: string;
}

type UserReqBody = Omit<
  IUser,
  "_id" | "createdAt" | "updatedAt" | "sessionRev"
> &
  UserPassword;

export const createUser = async (
  req: Request<any, any, UserReqBody>,
  res: ApiResponse,
  next: NextFunction
) => {
  const { email, username } = req.body;

  const userPropsSearch: IUser[] = await User.find({
    $or: [{ email }, { username }],
  });

  if (userPropsSearch.length !== 0) {
    const error: { username?: string; email?: string } = {};
    for (let user of userPropsSearch) {
      if (user.email === email) error.email = "Email already taken";
      if (user.username === username) error.email = "Username already taken";
    }

    res.status(400).json({
      code: 400,
      msg: "A user with this email or username already exists",
      error,
    });
    return next();
  }

  const { firstName, lastName, password, roles, avatar } = req.body;

  const date = new Date();
  const user: Omit<IUser, "_id" | "sessionRev"> & UserPassword = {
    firstName,
    lastName,
    email,
    username,
    roles,
    avatar,
    password,
    createdAt: date,
    updatedAt: date,
  };

  const newUser = await new User(user).save();

  if (newUser) {
    res.status(200).json({ code: 200, msg: "Success", data: newUser });

    return next();
  } else {
    res.status(500).json({ code: 500, msg: "Failed to create the user entry" });

    return next();
  }
};
