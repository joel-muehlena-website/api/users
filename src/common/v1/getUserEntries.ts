import { NextFunction, Request } from "express";
import { IUser } from "../../models/v1/user.interface";
import { User } from "../../models/v1/user";
import { ApiResponse } from "./ApiResponse";

export const getAllUsers = async (
  _: Request,
  res: ApiResponse,
  next: NextFunction
) => {
  const users: IUser[] = await User.find();

  if (users.length === 0) {
    res.status(404).json({ code: 404, msg: "No users found" });
    return next();
  }

  res.status(200).json({ code: 200, msg: "Success", data: users });
  return next();
};

export const getUserById = async (
  req: Request<{ id: string }>,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const user: IUser | undefined = await User.findById(id);

  if (!user) {
    res.status(404).json({ code: 404, msg: "No user with this id found" });
    return next();
  }

  res.status(200).json({ code: 200, msg: "Success", data: user });
  return next();
};

export const getRequestedUserList = async (
  req: Request<any, any, { requestedUsers: string[] }>,
  res: ApiResponse,
  next: NextFunction
) => {
  const { requestedUsers } = req.body;

  const users: IUser[] = await User.find({ _id: { $in: requestedUsers } });

  if (users.length === 0) {
    res.status(404).json({ code: 404, msg: "No users found" });
    return next();
  }

  res.status(200).json({ code: 200, msg: "Success", data: users });
  return next();
};
