import { NextFunction, Request } from "express";
import { IUser } from "src/models/v1/user.interface";
import { User } from "../../models/v1/user";
import { ApiResponse } from "./ApiResponse";

export const deleteUser = async (
  req: Request<{ id: string }>,
  res: ApiResponse,
  next: NextFunction
) => {
  const { id } = req.params;

  const user: IUser | undefined = await User.findByIdAndDelete(id);

  if (!user) {
    res
      .status(404)
      .json({ code: 404, msg: "No user with this id found cannot be deleted" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: user });
  return next();
};
